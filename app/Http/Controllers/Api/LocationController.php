<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Location\Department;
use App\Models\Location\Province;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function provinces(Department $department)
    {
        return $department->provinces()
            ->orderBy('name')
            ->get(['id', 'name']);
    }

    public function districts(Province $province)
    {
        return $province->districts()
            ->orderBy('name')
            ->get(['id', 'name']);
    }
}
