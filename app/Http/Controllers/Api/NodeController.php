<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Node;
use Illuminate\Http\Request;

class NodeController extends Controller
{
    public function show(Node $node)
    {
        $data = [];

        foreach ($node->node_fields as $node_field) {
            $data[$node_field->field->name] = $node_field->value;
        }
        return $data;
    }
}
