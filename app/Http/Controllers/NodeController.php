<?php

namespace App\Http\Controllers;

use App\Models\Field;
use App\Models\Location\Department;
use App\Models\Location\Province;
use App\Models\Node;
use App\Models\NodeField;
use App\Models\Template;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\File;

class NodeController extends Controller
{
    public function edit(Node $node)
    {
        $departmentName = $node->node_field('Departamento', Field::SELECT)->value;
        $provinceName = $node->node_field('Provincia', Field::SELECT)->value;

        $department = Department::firstWhere('name', $departmentName);
        $province = Province::firstWhere('name', $provinceName);

        $provinces = $department->provinces;
        $districts = $province->districts;

        $template = Template::where('type_node_domain', $node->type_domain)
            ->select('id')
            ->first();

        return view('nodes.edit', compact('node', 'template', 'provinces', 'districts'));
    }

    public function update(Node $node, Request  $request)
    {
        $rules = [];
        $arrays = [];

        $template = Template::where('type_node_domain', $node->type_domain)
            ->select('id')
            ->first();

        foreach ($template->fields as $field) {
            if ($field->validations) {
                $arrays[] =  [
                  'id' => $field->id,
                  'name' => $field->layout_name
                ];
                $rules[$field->layout_name] = $field->validations;
            }
            foreach ($field->options as $option) {
                if ($option->validations) {
                    $arrays[] =  [
                        'id' => $option->id,
                        'name' => $option->layout_name
                    ];
                    $rules[$option->layout_name] = $option->validations;
                }
                foreach ($option->options as $subOption) {
                    if ($subOption->validations) {
                        $arrays[] =  [
                            'id' => $subOption->id,
                            'name' => $subOption->layout_name
                        ];
                        $rules[$subOption->layout_name] = $subOption->validations;
                    }
                    foreach ($subOption->options as $label) {
                        if ($label->validations) {
                            $arrays[] =  [
                                'id' => $label->id,
                                'name' => $label->layout_name
                            ];
                            $rules[$label->layout_name] = $label->validations;
                        }

                        foreach ($label->options as $subLevel)
                            if ($subLevel->validations) {
                                $arrays[] =  [
                                    'id' => $subLevel->id,
                                    'name' => $subLevel->layout_name
                                ];
                                $rules[$subLevel->layout_name] = $subLevel->validations;
                            }
                    }
                }
            }
        }

        if ($request->has('value_images')) {
            $rules['images'] = 'nullable';
        }

        $this->validate($request, $rules);

        try {
            DB::beginTransaction();
            //node fields
//            dd($arrays);
            foreach ($arrays as $key => $array) {
                $id = $array['id'];
                $name = $array['name'];
                $requestInput = $request->input($name);
                if ($name == 'images' || $name == 'flats') {
                    $queryImages = $images = $node->node_fields()
                        ->where('field_id', $id);

                    if ($request->has('value_'.$name)) {
                        $images = $queryImages
                            ->whereNotIn('value', $request->input('value_'.$name))
                            ->get();
                    } else {
                        $images = $queryImages->get();
                    }
                    if ($images) {
                        foreach ($images as $image) {
                            $path = public_path('/images/'.$name.'/' . $image->value);
                            File::delete($path);

                            $image->delete();
                        }
                    }

                    if ($request->hasFile($name)) {
                        $archives = $request->file($name);

                        foreach ($archives as $archive) {
                            $extension = $archive->getClientOriginalExtension();
                            $file_name = uniqid() . '.' . $extension;

                            $path = public_path().'/images/'.$name;

                            $archive->move($path, $file_name);

                            NodeField::create([
                                'value' => $file_name,
                                'field_id' => $id,
                                'node_id' => $node->id
                            ]);
                        }
                    }

                } elseif (is_array($requestInput)) {
                    $node->node_fields()
                        ->where('field_id', $id)
                        ->wherenotIn('value', $requestInput)
                        ->delete();

                    foreach ($requestInput as $value) {
                        NodeField::updateOrCreate([
                            'value' => $value,
                            'field_id' => $id,
                            'node_id' => $node->id
                        ]);
                    }
                } else {
                    NodeField::updateOrCreate(
                        ['field_id' => $id, 'node_id' => $node->id],
                        ['value' => $requestInput]
                    );
                }
            }

            DB::commit();

            return redirect('templates/'.$template->id);
        } catch (Exception $e) {
            DB::rollback();

            return back()->with('error', $e->getMessage());
        }
    }
}
