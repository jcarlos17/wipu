<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MyPortalController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        return view('system.my-portal.index', compact('user'));
    }
}
