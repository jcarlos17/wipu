<?php

namespace App\Http\Controllers;

use App\Models\Location\Department;
use App\Models\Location\Province;
use App\Models\Node;
use App\Models\NodeField;
use App\Models\Template;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class TemplateController extends Controller
{
    public function index(Template $template)
    {
        $nodes = Node::where('type_domain', $template->type_node_domain)->paginate(10);

        $departments = Department::orderBy('name')
            ->get(['id', 'name']);

        return view('nodes.index', compact(
            'template', 'template', 'nodes', 'departments'
        ));
    }

    public function create(Template  $template)
    {
        $oldDepartment = old('department');
        $oldProvince = old('province');
        $provinces = $districts = [];

        if ($oldDepartment) {
            $department = Department::firstWhere('name', $oldDepartment);
            $provinces = $department->provinces;
        }

        if ($oldProvince) {
            $province = Province::firstWhere('name', $oldProvince);
            $districts = $province->districts;
        }

        return view('templates.create', compact(
            'template', 'provinces', 'districts'
        ));
    }

    public function store(Template $template, Request $request)
    {
        $rules = [];
        $arrays = [];

        foreach ($template->fields as $field) {
            if ($field->validations) {
                $arrays[] =  [
                    'id' => $field->id,
                    'name' => $field->layout_name
                ];
                $rules[$field->layout_name] = $field->validations;
            }
            foreach ($field->options as $option) {
                if ($option->validations) {
                    $arrays[] =  [
                        'id' => $option->id,
                        'name' => $option->layout_name
                    ];
                    $rules[$option->layout_name] = $option->validations;
                }
                foreach ($option->options as $subOption) {
                    if ($subOption->validations) {
                        $arrays[] =  [
                            'id' => $subOption->id,
                            'name' => $subOption->layout_name
                        ];
                        $rules[$subOption->layout_name] = $subOption->validations;
                    }
                    foreach ($subOption->options as $label) {
                        if ($label->validations) {
                            $arrays[] =  [
                                'id' => $label->id,
                                'name' => $label->layout_name
                            ];
                            $rules[$label->layout_name] = $label->validations;
                        }

                        foreach ($label->options as $subLevel)
                            if ($subLevel->validations) {
                                $arrays[] =  [
                                    'id' => $subLevel->id,
                                    'name' => $subLevel->layout_name
                                ];
                                $rules[$subLevel->layout_name] = $subLevel->validations;
                            }
                    }
                }
            }
        }
        $this->validate($request, $rules);

        try {
            DB::beginTransaction();

            $node = Node::create([
                'type_domain' => $template->type_node_domain
            ]);

            //node fields
            $order = 1;
            foreach ($arrays as $key => $array) {
                $id = $array['id'];
                $name = $array['name'];

                $requestInput = $request->input($name);
                if ($request->hasFile($name)) {
                    $archives = $request->file($name);

                    foreach ($archives as $archive) {
                        $extension = $archive->getClientOriginalExtension();
                        $file_name = uniqid() . '.' . $extension;

                        $path = public_path().'/images/'.$name;

                        $archive->move($path, $file_name);

                        NodeField::create([
                            'value' => $file_name,
                            'field_id' => $id,
                            'node_id' => $node->id
                        ]);
                    }
                } elseif (is_array($requestInput)) {
                    foreach ($requestInput as $value) {
                        NodeField::create([
                            'value' => $value,
                            'field_id' => $id,
                            'node_id' => $node->id
                        ]);
                    }
                } else {
                    NodeField::create([
                        'value' => $requestInput,
                        'order' => $order,
                        'field_id' => $id,
                        'node_id' => $node->id
                    ]);
                    $order++;
                }
            }

            DB::commit();

            return redirect('templates/'.$template->id);
        } catch (Exception $e) {
            DB::rollback();

            return back()->with('error', $e->getMessage());
        }
    }
}
