<?php

namespace App\Models;

use App\Models\Location\Department;
use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    const LABEL = 1;
    const TEXT = 2;
    const SELECT = 3;
    const CHECKBOX = 4;
    const TEXTAREA = 5;
    const FILE = 6;
    const IMAGE = 7;
    const RADIO = 8;
    const NAV_TAP = 9;
    const TAB = 10;
    const MAP = 11;

    public function options()
    {
        return $this->hasMany(Field::class, 'parent_id');
    }

    public function node_fields()
    {
        return $this->hasMany(NodeField::class);
    }

    public function node_field_value($nodeId)
    {
        if (!$nodeId)
            return '';

        $query = $this->node_fields()
            ->where('field_id', $this->id)
            ->where('node_id', $nodeId);

        if ($query->count() > 1 || $this->type_domain == Field::CHECKBOX || $this->type_domain == Field::IMAGE) {
            return $query->pluck('value')->toArray();

        } else {
            $nodeField = $query->first(['value']);

            if (!$nodeField)
                return '';

            return $nodeField->value;
        }
    }

    public function getDataOptionsAttribute()
    {
        if ($this->getLayoutNameAttribute() == 'department')
            return Department::all();

        return $this->options;
    }

    public function getColAttribute()
    {
        $obj = json_decode($this->json_param);

        if (!isset($obj->{'layout'}))
            return 12;

        $layout = $obj->{'layout'};

        if (!$layout || !$layout->{'col'})
            return 12;

        return $layout->{'col'};
    }

    public function getTypeAttribute()
    {
        $obj = json_decode($this->json_param);

        if (!isset($obj->{'layout'}))
            return 'text';

        $layout = $obj->{'layout'};

        if(!isset($layout->{'type'}))
            return 'text';

        return $layout->{'type'};
    }

    public function getClearfixAttribute()
    {
        $obj = json_decode($this->json_param);

        if (!isset($obj->{'layout'}))
            return '';

        $layout = $obj->{'layout'};

        if(!isset($layout->{'clearfix'}))
            return '';

        return $layout->{'clearfix'} ? '<div class="clearfix"></div>' : '';
    }

    public function getLayoutNameAttribute()
    {
        $obj = json_decode($this->json_param);

        if (!isset($obj->{'layout'}))
            return '';

        $layout = $obj->{'layout'};

        if(!isset($layout->{'name'}))
            return '';

        return $layout->{'name'};
    }

    public function getLayoutWidthAttribute()
    {
        $obj = json_decode($this->json_param);

        if (!isset($obj->{'layout'}))
            return '';

        $layout = $obj->{'layout'};

        if(!isset($layout->{'width'}))
            return '';

        return $layout->{'width'};
    }

    public function getLayoutHeightAttribute()
    {
        $obj = json_decode($this->json_param);

        if (!isset($obj->{'layout'}))
            return '';

        $layout = $obj->{'layout'};

        if(!isset($layout->{'height'}))
            return '';

        return $layout->{'height'};
    }

    public function getLayoutSizeAttribute()
    {
        $obj = json_decode($this->json_param);

        if (!isset($obj->{'layout'}))
            return '';

        $layout = $obj->{'layout'};

        if(!isset($layout->{'size'}))
            return '';

        return $layout->{'size'};
    }

    public function getLayoutSuffixAttribute()
    {
        $obj = json_decode($this->json_param);

        if (!isset($obj->{'layout'}))
            return '';

        $layout = $obj->{'layout'};

        if(!isset($layout->{'suffix'}))
            return '';

        return $layout->{'suffix'};
    }

    public function getLayoutApiUrlAttribute()
    {
        $obj = json_decode($this->json_param);

        if (!isset($obj->{'layout'}))
            return false;

        $layout = $obj->{'layout'};

        if(!isset($layout->{'api_url'}))
            return false;

        return $layout->{'api_url'};
    }

    public function getLayoutDisabledElementsAttribute()
    {
        $obj = json_decode($this->json_param);

        if (!isset($obj->{'layout'}))
            return [];

        $layout = $obj->{'layout'};

        if(!isset($layout->{'disabled_elements'}))
            return [];

        return $layout->{'disabled_elements'};
    }

    public function getLayoutEffectedElementAttribute()
    {
        $obj = json_decode($this->json_param);

        if (!isset($obj->{'layout'}))
            return false;

        $layout = $obj->{'layout'};

        if(!isset($layout->{'effected_element'}))
            return false;

        return $layout->{'effected_element'};
    }

    public function getLayoutClassAttribute()
    {
        $obj = json_decode($this->json_param);

        if (!isset($obj->{'layout'}))
            return false;

        $layout = $obj->{'layout'};

        if(!isset($layout->{'class'}))
            return false;

        return $layout->{'class'};
    }

    public function getLayoutPlaceholderAttribute()
    {
        $obj = json_decode($this->json_param);

        if (!isset($obj->{'layout'}))
            return '';

        $layout = $obj->{'layout'};

        if(!isset($layout->{'placeholder'}))
            return '';

        return $layout->{'placeholder'};
    }

    public function getValidationsAttribute()
    {
        $obj = json_decode($this->json_param);

        if (!isset($obj->{'validations'}))
            return '';

        return $obj->{'validations'};
    }
}
