<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    protected $fillable = [
        'ubigee', 'name'
    ];

    public function provinces()
    {
        return $this->hasMany(Province::class);
    }
}
