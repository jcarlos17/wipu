<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use HasFactory;

    protected $fillable = [
        'ubigee', 'name', 'department_id'
    ];

    public function districts()
    {
        return $this->hasMany(District::class);
    }
}
