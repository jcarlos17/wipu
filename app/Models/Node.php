<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Node extends Model
{
    //type_domain
    const PROJECT = 1;
    const PROPERTY = 2;
    const ROOMMATE = 3;
    const WEB = 4;

    protected $fillable = [
        'type_domain', 'parent_id'
    ];

    public function fields()
    {
        return $this->belongsToMany(Field::class, 'node_field', 'node_id', 'field_id')
            ->orderBy('node_field.order');
    }

    public function node_fields()
    {
        return $this->hasMany(NodeField::class);
    }

    public function node_field($field, $type)
    {
        return $this->node_fields()
            ->with('field')
            ->whereHas('field', function($query) use ($field, $type) {
                $query->where('name', $field)
                ->where('type_domain', $type);
            })
            ->first();
    }
}
