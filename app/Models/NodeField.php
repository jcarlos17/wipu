<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NodeField extends Model
{
    protected $table = 'node_field';

    protected $fillable = [
        'value', 'order', 'field_id', 'node_id'
    ];

    public function field()
    {
        return $this->belongsTo(Field::class);
    }

    public function node()
    {
        return $this->belongsTo(Node::class);
    }
}
