<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NodeUser extends Model
{
    protected $table = 'node_user';

    //state_domain
    //only clients
    const CONTACT = 1;
    const PROSPECTUS = 2;
    const VISITING = 3;
    const NEGOTIATION = 4;
    const CLOSING = 5;
    //only collector
    const INACTIVE = 6;
    const WON = 7;


}
