<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    //type_node_domain
    const PROJECT = 1;
    const PROPERTY = 2;
    const ROOMMATE = 3;
    const WEB = 4;
    const BLOG = 5;
    /**
     * @var mixed
     */

    public function fields()
    {
        return $this->belongsToMany(Field::class, 'template_field', 'template_id', 'field_id')
            ->orderBy('template_field.order');
    }
}
