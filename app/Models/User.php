<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    CONST ROLE_GENERAL_ADMIN = 0;
    CONST ROLE_PROPERTY_ADMIN = 1;

    public function is($role)
    {
        return $this->role == $role;
    }

    public function getPhotoUrlAttribute()
    {
        return '/images/users/'.$this->photo;
    }

    public function getRoleNameAttribute()
    {
        if($this->role == 0)
            return 'Administrador general';
        else
            return 'Administrador de inmuebles';
    }
}
