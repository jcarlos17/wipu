<?php
namespace Database\Seeders;

use App\Models\Field;
use App\Models\TemplateField;
use Illuminate\Database\Seeder;

class FieldsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //tabs property
        $property = Field::create([
            'name' => 'Tab Inmuebles',
            'type_domain' => Field::NAV_TAP,
        ]);

        $tabMainData = Field::create([
            'name' => 'Datos principales',
            'type_domain' => Field::TAB,
            'parent_id' => $property->id,
            'json_param' => '{"layout":{"class": "in active", "aria-expanded": true, "name": "tab_main_data"}}'
        ]);

        $this->createMainDataProperty($tabMainData->id);

        $tabCharacteristic = Field::create([
            'name' => 'Características',
            'type_domain' => Field::TAB,
            'parent_id' => $property->id,
            'json_param' => '{"layout":{"name": "tab_characteristic"}}'
        ]);

        $this->createCharacteristicProperty($tabCharacteristic->id);

        $tabMultimedia = Field::create([
            'name' => 'Multimedia',
            'type_domain' => Field::TAB,
            'parent_id' => $property->id,
            'json_param' => '{"layout":{"name": "tab_multimedia"}}'
        ]);

        $this->createImages($tabMultimedia->id);
        $this->createMultimediaProperty($tabMultimedia->id);

        $tabAssessor = Field::create([
            'name' => 'Asesor',
            'type_domain' => Field::TAB,
            'parent_id' => $property->id,
            'json_param' => '{"layout":{"name": "tab_assessor"}}'
        ]);

        $this->createAssessor($tabAssessor->id);

        $tabProperty = Field::create([
            'name' => 'Propietario',
            'type_domain' => Field::TAB,
            'parent_id' => $property->id,
            'json_param' => '{"layout":{"name": "tab_property"}}'
        ]);

        $this->createPropertyProject($tabProperty->id);

        $transaction = Field::create([
            'name' => 'Transacción',
            'type_domain' => Field::CHECKBOX,
            'parent_id' => $tabProperty->id,
            'json_param' => '{"layout":{"col": 12, "name": "transaction"},"validations" : ["required"]}'
        ]);
        Field::create(['name' => 'Exclusividad', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 12}}', 'parent_id' => $transaction->id]);
        Field::create(['name' => 'Letrero', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 12}}', 'parent_id' => $transaction->id]);
        Field::create(['name' => 'Disponibilidad', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 12}}', 'parent_id' => $transaction->id]);
        Field::create([
            'name' => 'Fecha de vencimiento',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 12, "type": "date", "name": "expiration_date"}, "validations": ["nullable", "date"]}',
            'parent_id' => $transaction->id
        ]);
        $commission = Field::create([
            'name' => 'Comisión (%)',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 12, "type": "number", "name": "commission_number"},"validations" : ["nullable"]}',
            'parent_id' => $transaction->id
        ]);
        Field::create([
            'name' => 'Incluye IGV',
            'type_domain' => Field::LABEL,
            'json_param' => '{"layout":{"col": 12}}',
            'parent_id' => $commission->id
        ]);
        $period = Field::create([
            'name' => 'Periodo',
            'type_domain' => Field::SELECT,
            'json_param' => '{"layout":{"col": 12, "name": "period"},"validations" : ["nullable"]}',
            'parent_id' => $commission->id
        ]);
        Field::create(['name' => 'Año', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $period->id]);
        Field::create(['name' => 'Mes', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $period->id]);

        $contract = Field::create([
            'name' => 'Tipo de Contrato',
            'type_domain' => Field::SELECT,
            'parent_id' => $tabProperty->id,
            'json_param' => '{"layout":{"col": 3, "name": "contract_type"},"validations" : ["required"]}'
        ]);
        Field::create(['name' => 'Correo', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": 0}}', 'parent_id' => $contract->id]);
        Field::create(['name' => 'Físico', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": 0}}', 'parent_id' => $contract->id]);

        //tabs projects
        $project = Field::create([
            'name' => 'Tab Proyecto',
            'type_domain' => Field::NAV_TAP,
        ]);

        $tabMainDataProject = Field::create([
            'name' => 'Datos principales',
            'type_domain' => Field::TAB,
            'parent_id' => $project->id,
            'json_param' => '{"layout":{"class": "in active", "aria-expanded": true, "name": "tab_main_data"}}'
        ]);

        $this->createMainDataProject($tabMainDataProject->id);

        $tabCharacteristicProject = Field::create([
            'name' => 'Características',
            'type_domain' => Field::TAB,
            'parent_id' => $project->id,
            'json_param' => '{"layout":{"name": "tab_characteristic"}}'
        ]);

        $this->createCharacteristicProject($tabCharacteristicProject->id);

        $tabMultimediaProject = Field::create([
            'name' => 'Multimedia',
            'type_domain' => Field::TAB,
            'parent_id' => $project->id,
            'json_param' => '{"layout":{"name": "tab_multimedia"}}'
        ]);

        $this->createImages($tabMultimediaProject->id);

        $tabAssessorProject = Field::create([
            'name' => 'Asesor',
            'type_domain' => Field::TAB,
            'parent_id' => $project->id,
            'json_param' => '{"layout":{"name": "tab_assessor"}}'
        ]);

        $this->createAssessor($tabAssessorProject->id);

        $tabContact = Field::create([
            'name' => 'Contacto',
            'type_domain' => Field::TAB,
            'parent_id' => $project->id,
            'json_param' => '{"layout":{"name": "tab_contact"}}'
        ]);

        $this->createPropertyProject($tabContact->id);

        $transaction = Field::create([
            'name' => 'Transacción',
            'type_domain' => Field::CHECKBOX,
            'parent_id' => $tabContact->id,
            'json_param' => '{"layout":{"col": 12, "name": "transaction"},"validations" : ["required"]}'
        ]);
        Field::create(['name' => 'Exclusividad', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 12}}', 'parent_id' => $transaction->id]);
        Field::create(['name' => 'Letrero', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 12}}', 'parent_id' => $transaction->id]);
        Field::create(['name' => 'Disponibilidad', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 12}}', 'parent_id' => $transaction->id]);
        Field::create([
            'name' => 'Fecha de vencimiento',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 12, "type": "date", "name": "expiration_date"}, "validations": ["nullable", "date"]}',
            'parent_id' => $transaction->id
        ]);
        $commission = Field::create([
            'name' => 'Comisión (%)',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 12, "type": "number", "name": "commission_number"},"validations" : ["nullable"]}',
            'parent_id' => $transaction->id
        ]);
        Field::create([
            'name' => 'Incluye IGV',
            'type_domain' => Field::LABEL,
            'json_param' => '{"layout":{"col": 12}}',
            'parent_id' => $commission->id
        ]);

        $contract = Field::create([
            'name' => 'Tipo de Contrato',
            'type_domain' => Field::SELECT,
            'parent_id' => $tabContact->id,
            'json_param' => '{"layout":{"col": 3, "name": "contract_type"},"validations" : ["required"]}'
        ]);
        Field::create(['name' => 'Correo', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": 0}}', 'parent_id' => $contract->id]);
        Field::create(['name' => 'Físico', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": 0}}', 'parent_id' => $contract->id]);
        //template field
        TemplateField::create(['order' => 1, 'template_id' => 1, 'field_id' => $property->id]);
        TemplateField::create(['order' => 1, 'template_id' => 2, 'field_id' => $project->id]);
    }
    //properties
    function createMainDataProperty($tabMainDataId)
    {
        $this->createTitleDescription($tabMainDataId);
        $this->createPropertyType($tabMainDataId);

        $antiquity = Field::create([
            'name' => 'Antigüedad',
            'type_domain' => Field::RADIO,
            'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 12, "name": "antiquity"},"validations" : ["required"]}'
        ]);

        Field::create(['name' => 'En construcción', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 3}}', 'parent_id' => $antiquity->id]);
        Field::create(['name' => 'A estrenar', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 3}}', 'parent_id' => $antiquity->id]);
        Field::create([
            'name' => 'En años',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "in_years", "suffix": "años"},"validations" : ["required_if:antiquity,==,En años"]}',
            'parent_id' => $antiquity->id
        ]);

        $this->createPropertyQuantity($tabMainDataId);

        $price = Field::create([
            'name' => 'Precio',
            'type_domain' => Field::CHECKBOX,
            'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 12, "name": "price"},"validations" : ["nullable"]}'
        ]);

        $sell = Field::create([
            'name' => 'Anunciar para vender',
            'type_domain' => Field::CHECKBOX,
            'json_param' => '{"layout":{"col": 3, "name": "sell"},"validations" : ["nullable"]}',
            'parent_id' => $price->id
        ]);
        Field::create([
            'name' => 'S/',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "sell_pen"},"validations" : ["nullable"]}',
            'parent_id' => $sell->id
        ]);
        Field::create([
            'name' => 'US$',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "sell_usd"},"validations" : ["nullable"]}',
            'parent_id' => $sell->id
        ]);

        $rent = Field::create([
            'name' => 'Anunciar para alquilar',
            'type_domain' => Field::CHECKBOX,
            'json_param' => '{"layout":{"col": 3, "name": "rent"},"validations" : ["nullable"]}',
            'parent_id' => $price->id
        ]);
        Field::create([
            'name' => 'S/',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "rent_pen"},"validations" : ["nullable"]}',
            'parent_id' => $rent->id
        ]);
        Field::create([
            'name' => 'US$',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "rent_usd"},"validations" : ["nullable"]}',
            'parent_id' => $rent->id
        ]);

        $season = Field::create([
            'name' => 'Anunciar para temporada',
            'type_domain' => Field::CHECKBOX,
            'json_param' => '{"layout":{"col": 3, "name": "season"},"validations" : ["nullable"]}',
            'parent_id' => $price->id
        ]);
        Field::create([
            'name' => 'S/',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "season_pen"},"validations" : ["nullable"]}',
            'parent_id' => $season->id
        ]);
        Field::create([
            'name' => 'US$',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "season_usd"},"validations" : ["nullable"]}',
            'parent_id' => $season->id
        ]);

        $transfer = Field::create([
            'name' => 'Anunciar para traspaso',
            'type_domain' => Field::CHECKBOX,
            'json_param' => '{"layout":{"col": 3, "name": "transfer"},"validations" : ["nullable"]}',
            'parent_id' => $price->id
        ]);
        Field::create([
            'name' => 'S/',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "transfer_pen"},"validations" : ["nullable"]}',
            'parent_id' => $transfer->id
        ]);
        Field::create([
            'name' => 'US$',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "transfer_usd"},"validations" : ["nullable"]}',
            'parent_id' => $transfer->id
        ]);

        Field::create([
            'name' => 'Mantenimiento', 'type_domain' => Field::TEXT, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 3, "type": "number", "clearfix": 1, "name": "maintenance", "placeholder": "Mantenimiento"},"validations" : ["nullable", "numeric", "min:1"]}'
        ]);
        Field::create([
            'name' => 'Metros construidos', 'type_domain' => Field::TEXT, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "build_meter", "placeholder": "Metros construidos"},"validations" : ["nullable", "numeric", "min:1"]}'
        ]);
        Field::create([
            'name' => 'Metros totales', 'type_domain' => Field::TEXT, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 3, "type": "number", "clearfix": 1, "name": "total_meter", "placeholder": "Metros totales"},"validations" : ["required", "numeric", "min:1"]}'
        ]);
        Field::create([
            'name' => 'Frente m2', 'type_domain' => Field::TEXT, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "front_m2", "placeholder": "Frente m2"},"validations" : ["required", "numeric", "min:1"]}'
        ]);
        Field::create([
            'name' => 'Fondo m2',
            'type_domain' => Field::TEXT, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 3, "type": "number", "clearfix": 1, "name": "depth_m2", "placeholder": "Fondo m2"},"validations" : ["required", "numeric", "min:1"]}'
        ]);

        $this->createLocation($tabMainDataId);
    }

    function createCharacteristicProperty($tabCharacteristicId)
    {
        $generalCharacteristic = Field::create([
            'name' => 'Características generales',
            'type_domain' => Field::CHECKBOX,
            'parent_id' => $tabCharacteristicId,
            'json_param' => '{"layout":{"col": 12, "name": "general_characteristic"},"validations" : ["nullable"]}'
        ]);

        Field::create(['name' => 'Triplex', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'En condominio', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Reposteros en cocina', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Vistas al mar', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Frente al mar', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Jacuzzi', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Terraza', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Jardin', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Amoblado', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Baño de servicio', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Cuartos de servicio', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Walk in closet', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Frente a parque', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Vista a la ciudad', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Duplex', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Closet', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Flat', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Penthouse', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Azotea', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Bar', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);
        Field::create(['name' => 'Depósitos', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $generalCharacteristic->id]);

        $this->createServiceAreaExterior($tabCharacteristicId);
    }

    function createImages($tabMultimediaId)
    {
        Field::create([
            'name' => 'Agregar imágenes',
            'type_domain' => Field::IMAGE,
            'parent_id' => $tabMultimediaId,
            'json_param' => '{"layout":{"col": 12, "name": "images", "width": 1920, "height": 1357, "size": 3072},"validations" : ["required"]}'
        ]);
        Field::create([
            'name' => 'Agregar planos',
            'type_domain' => Field::IMAGE,
            'parent_id' => $tabMultimediaId,
            'json_param' => '{"layout":{"col": 12, "name": "flats", "width": 1920, "height": 720, "size": 3072},"validations" : ["nullable"]}'
        ]);
    }

    function createMultimediaProperty($tabMultimediaId)
    {
        Field::create([
            'name' => 'Agregar videos',
            'type_domain' => Field::TEXT,
            'parent_id' => $tabMultimediaId,
            'json_param' => '{"layout":{"col": 12, "name": "add_video", "placeholder": "Ingresar URL de: youtube, vimeo o dailymotion"},"validations" : ["nullable", "min:3", "max:255"]}'
        ]);
        Field::create([
            'name' => 'Agregar vision 360°',
            'type_domain' => Field::TEXT,
            'parent_id' => $tabMultimediaId,
            'json_param' => '{"layout":{"col": 12, "name": "add_vision", "placeholder": "Ingresar URL"},"validations" : ["nullable", "min:3", "max:255"]}'
        ]);
    }

    function createAssessor($tabAssessorId)
    {
        $assessor = Field::create([
            'name' => 'Asesor',
            'type_domain' => Field::SELECT,
            'parent_id' => $tabAssessorId,
            'json_param' => '{"layout":{"col": "3", "name": "assessor"},"validations" : ["required"]}'
        ]);

        Field::create(['name' => 'Maria Castillo', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $assessor->id]);
    }

    function createPropertyProject($tabPropertyId)
    {
        Field::create([
            'name' => 'Nombre', 'type_domain' => Field::TEXT, 'parent_id' => $tabPropertyId,
            'json_param' => '{"layout":{"col": 4, "name": "property_name", "placeholder": "Nombre propietario"},"validations" : ["required", "max:255"]}'
        ]);
        Field::create([
            'name' => 'Teléfono', 'type_domain' => Field::TEXT, 'parent_id' => $tabPropertyId,
            'json_param' => '{"layout":{"col": 4, "name": "property_phone", "placeholder": "Teléfono propietario"},"validations" : ["required", "max:255"]}'
        ]);
        Field::create([
            'name' => 'Correo', 'type_domain' => Field::TEXT, 'parent_id' => $tabPropertyId,
            'json_param' => '{"layout":{"col": 4, "name": "property_email", "placeholder": "Correo propietario"},"validations" : ["required", "max:255"]}'
        ]);
        $pages = Field::create([
            'name' => 'Páginas',
            'type_domain' => Field::CHECKBOX,
            'parent_id' => $tabPropertyId,
            'json_param' => '{"layout":{"col": 12, "name": "pages"},"validations" : ["required"]}'
        ]);
        Field::create(['name' => 'Adondevivir', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 12}}', 'parent_id' => $pages->id]);
        Field::create(['name' => 'Urbania', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 12}}', 'parent_id' => $pages->id]);
        Field::create(['name' => 'Hol', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 12}}', 'parent_id' => $pages->id]);
        Field::create(['name' => 'Aspai', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 12}}', 'parent_id' => $pages->id]);
    }

    //projects
    function createMainDataProject($tabMainDataId)
    {
        $this->createTitleDescription($tabMainDataId);

        $selectProject = Field::create([
            'name' => 'Etapa de Proyecto',
            'type_domain' => Field::SELECT,
            'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": "3", "name": "project_stage"},"validations" : ["required"]}'
        ]);

        Field::create(['name' => 'Pre-venta en planos', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $selectProject->id]);
        Field::create(['name' => 'Pre-venta en construcción', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $selectProject->id]);
        Field::create(['name' => 'Venta en estreno', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $selectProject->id]);

        $delivery = Field::create([
            'name' => 'Entrega',
            'type_domain' => Field::SELECT,
            'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": "3", "name": "project_stage"},"validations" : ["required"]}'
        ]);

        Field::create(['name' => '1° trimestre', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $delivery->id]);
        Field::create(['name' => '2° trimestre', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $delivery->id]);
        Field::create(['name' => '3° trimestre', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $delivery->id]);
        Field::create(['name' => '4° trimestre', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $delivery->id]);
        Field::create(['name' => 'Enero', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $delivery->id]);
        Field::create(['name' => 'Febrero', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $delivery->id]);
        Field::create(['name' => 'Marzo', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $delivery->id]);
        Field::create(['name' => 'Abril', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $delivery->id]);
        Field::create(['name' => 'Mayo', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $delivery->id]);
        Field::create(['name' => 'Junio', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $delivery->id]);
        Field::create(['name' => 'Julio', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $delivery->id]);
        Field::create(['name' => 'Agosto', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $delivery->id]);
        Field::create(['name' => 'Septiembre', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $delivery->id]);
        Field::create(['name' => 'Octubre', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $delivery->id]);
        Field::create(['name' => 'Noviembre', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $delivery->id]);
        Field::create(['name' => 'Diciembre', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"default": "0"}}', 'parent_id' => $delivery->id]);

        Field::create([
            'name' => 'Año de entrega (YYYY)', 'type_domain' => Field::TEXT, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "bedroom", "placeholder": "Año de entrega (YYYY)"},"validations" : ["required", "numeric", "min:1"]}'
        ]);

        $this->createPropertyType($tabMainDataId);

        $this->createPropertyQuantity($tabMainDataId);

        Field::create([
            'name' => 'N° Pisos',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "floor_number", "placeholder": "N° Pisos"},"validations" : ["required", "numeric", "min:1"]}'
        ]);
        Field::create([
            'name' => 'N° Departamentos',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 3, "type": "number", "clearfix": 1, "name": "department_number", "placeholder": "N° Departamentos"},"validations" : ["required", "numeric", "min:1"]}'
        ]);
        Field::create([
            'name' => 'Precio desde S/',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "pen_price_from", "placeholder": "Precio desde S/"},"validations" : ["required", "numeric", "min:1"]}'
        ]);
        Field::create([
            'name' => 'Precio desde US$',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 3, "type": "number", "clearfix": 1, "name": "usd_price_from", "placeholder": "Precio desde US$"},"validations" : ["required", "numeric", "min:1"]}'
        ]);
        Field::create([
            'name' => 'Área total mínima',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "min_total_area", "placeholder": "Área total mínima"},"validations" : ["required", "numeric", "min:1"]}'
        ]);
        Field::create([
            'name' => 'Área total máxima',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 3, "type": "number", "clearfix": 1, "name": "max_total_area", "placeholder": "Área total máxima"},"validations" : ["required", "numeric", "min:1"]}'
        ]);
        Field::create([
            'name' => 'Área techada mínima',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "min_covered_area", "placeholder": "Área techada mínima"},"validations" : ["required", "numeric", "min:1"]}'
        ]);
        Field::create([
            'name' => 'Área techada máxima',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 3, "type": "number", "clearfix": 1, "name": "max_covered_area", "placeholder": "Área techada máxima"},"validations" : ["required", "numeric", "min:1"]}'
        ]);

        $this->createLocation($tabMainDataId);
    }

    function createCharacteristicProject($tabCharacteristicId)
    {
        $this->createServiceAreaExterior($tabCharacteristicId);
    }

    function createTitleDescription($tabMainDataId)
    {
        Field::create([
            'name' => 'Título', 'type_domain' => Field::TEXT,
            'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 12, "name": "title", "placeholder": "Incluya el tipo de propiedad y su característica principal"},"validations" : ["required", "min:3", "max:255"]}'
        ]);

        Field::create([
            'name' => 'Descripción', 'type_domain' => Field::TEXTAREA, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 12, "name": "description"},"validations" : ["required", "min:3", "max:1250"]}'
        ]);
    }
    function createPropertyType($tabMainDataId)
    {
        $radio = Field::create([
            'name' => 'Tipo de inmueble', 'type_domain' => Field::RADIO, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 12, "name": "property_type"},"validations" : ["required"]}'
        ]);

        Field::create(['name' => 'Casa', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 3}}', 'parent_id' => $radio->id]);
        Field::create(['name' => 'Casa de Campo', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 3}}', 'parent_id' => $radio->id]);
        Field::create(['name' => 'Casa de playa', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 3}}', 'parent_id' => $radio->id]);
        Field::create(['name' => 'Departamento', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 3}}', 'parent_id' => $radio->id]);
        Field::create(['name' => 'Habitación', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 3}}', 'parent_id' => $radio->id]);
        Field::create(['name' => 'Oficina', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 3}}', 'parent_id' => $radio->id]);
        Field::create(['name' => 'Terreno / Lote', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 3}}', 'parent_id' => $radio->id]);
        Field::create(['name' => 'Local Comercial', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 3}}', 'parent_id' => $radio->id]);
        Field::create(['name' => 'Local Industrial', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 3}}', 'parent_id' => $radio->id]);
        Field::create(['name' => 'Terreno agrícola', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 3}}', 'parent_id' => $radio->id]);
        Field::create(['name' => 'Negocio en marcha', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 3}}', 'parent_id' => $radio->id]);
        Field::create(['name' => 'Otros', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 3}}', 'parent_id' => $radio->id]);
    }
    function createPropertyQuantity($tabMainDataId)
    {
        Field::create([
            'name' => 'Dormitorios', 'type_domain' => Field::TEXT, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "bedroom", "placeholder": "Dormitorios"},"validations" : ["required", "numeric", "min:1"]}'
        ]);
        Field::create([
            'name' => 'Estacionamientos', 'type_domain' => Field::TEXT, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "parking", "placeholder": "Estacionamientos"},"validations" : ["nullable", "numeric", "min:1"]}'
        ]);
        Field::create([
            'name' => 'Baños', 'type_domain' => Field::TEXT, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "bath", "placeholder": "Baños"},"validations" : ["required", "numeric", "min:1"]}'
        ]);
        Field::create([
            'name' => 'Medio baño', 'type_domain' => Field::TEXT, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 3, "type": "number", "name": "half_bath", "placeholder": "Medio baño"},"validations" : ["required", "numeric", "min:1"]}'
        ]);
        Field::create([
            'name' => 'N° camas', 'type_domain' => Field::TEXT, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 3, "type": "number", "clearfix": 1, "name": "bed_number", "placeholder": "N° camas"},"validations" : ["nullable", "numeric", "min:1"]}'
        ]);
    }
    function createLocation($tabMainDataId)
    {
        Field::create([
            'name' => 'Departamento', 'type_domain' => Field::SELECT, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": "4", "name": "department", "api_url": "/api/department/provinces/", "disabled_elements": ["province","district"], "effected_element": "province"},"validations" : ["required"]}'
        ]);
        Field::create([
            'name' => 'Provincia', 'type_domain' => Field::SELECT, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": "4", "name": "province", "api_url": "/api/province/districts/", "disabled_elements": ["district"], "effected_element": "district"},"validations" : ["required"]}'
        ]);
        Field::create([
            'name' => 'Distrito', 'type_domain' => Field::SELECT, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": "4", "name": "district"},"validations" : ["required"]}'
        ]);

        Field::create([
            'name' => 'Dirección',
            'type_domain' => Field::TEXT, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 12, "name": "address", "placeholder": "Dirección"},"validations" : ["required", "min:3", "max:255"]}'
        ]);

        Field::create([
            'name' => 'Mapa',
            'type_domain' => Field::MAP, 'parent_id' => $tabMainDataId,
            'json_param' => '{"layout":{"col": 12, "name": "mapid"},"validations" : ["required", "max:255"]}'
        ]);
    }
    function createServiceAreaExterior($tabCharacteristicId)
    {
        $services = Field::create([
            'name' => 'Servicios',
            'type_domain' => Field::CHECKBOX,
            'parent_id' => $tabCharacteristicId,
            'json_param' => '{"layout":{"col": 12, "name": "services"},"validations" : ["nullable"]}'
        ]);
        Field::create(['name' => 'Gimnasio', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Parrilla', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Canchas Deportivas', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Juegos infantiles', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Áreas verdes', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Sala de computos', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Video vigilancia', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Control de accesos', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Área de lavandería', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Televisión por cable', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Servicios basicos(agua/luz)', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Cancha de futbol', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Cancha de tenis', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Portero electrico', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Cerca a colegios', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Recepción', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create([
            'name' => 'Número de pisos',
            'type_domain' => Field::TEXT,
            'json_param' => '{"layout":{"col": 6, "name": "floor_number"},"validations" : ["nullable", "numeric"]}',
            'parent_id' => $services->id
        ]);
        Field::create(['name' => 'Aceptan mascotas', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Caseta de guardia', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Intercomunicador', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Cerca a parque', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Centros comerciales cercanos', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);
        Field::create(['name' => 'Ascensor', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $services->id]);

        //area
        $area = Field::create([
            'name' => 'Áreas comunes',
            'type_domain' => Field::CHECKBOX,
            'parent_id' => $tabCharacteristicId,
            'json_param' => '{"layout":{"col": 12, "name": "common_area"},"validations" : ["required"]}'
        ]);
        Field::create(['name' => 'Sala de entretenimiento', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $area->id]);
        Field::create(['name' => 'Bodega', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $area->id]);
        Field::create(['name' => 'Sauna', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $area->id]);
        Field::create(['name' => 'Patio', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $area->id]);
        Field::create(['name' => 'Área de cafeteria', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $area->id]);
        Field::create(['name' => 'Piscina', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $area->id]);

        //exterior
        $area = Field::create([
            'name' => 'Exteriores',
            'type_domain' => Field::CHECKBOX,
            'parent_id' => $tabCharacteristicId,
            'json_param' => '{"layout":{"col": 12, "name": "exterior"},"validations" : ["nullable"]}'
        ]);
        Field::create(['name' => 'Balcón', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $area->id]);
        Field::create(['name' => 'Área de BBQ', 'type_domain' => Field::LABEL, 'json_param' => '{"layout":{"col": 6}}', 'parent_id' => $area->id]);
    }
}
