<?php

namespace Database\Seeders;

use App\Models\Location\Department;
use App\Models\Location\District;
use App\Models\Location\Province;
use Exception;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();
            $file = public_path('data/Ubigeo-2016.csv');
            $lines = file($file);
            $utf8_lines = array_map('utf8_encode', $lines);
            $array = array_map(function($v){return str_getcsv($v, ";");}, $utf8_lines);

            for ($i=1; $i<sizeof($array); ++$i){
                // Fetch data
                $ubigee = $array[$i][0];
                $departmentName = $array[$i][1];
                $provinceName = $array[$i][2];
                $district = $array[$i][3];

                $department = Department::firstOrCreate(
                    ['name' => $departmentName],
                    ['ubigee' => $ubigee]
                );

                if ($provinceName != '')
                    $province = Province::firstOrCreate(
                        ['name' => $provinceName, 'department_id' => $department->id],
                        ['ubigee' => $ubigee]
                    );

                if ($district != '')
                    District::firstOrCreate(
                        ['name' => $district, 'province_id' => $province->id],
                        ['ubigee' => $ubigee]
                    );


            }
            DB::commit();

        } catch (Exception $e) {
            DB::rollback();
        }
    }
}
