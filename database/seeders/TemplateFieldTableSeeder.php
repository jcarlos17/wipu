<?php
namespace Database\Seeders;

use App\Models\TemplateField;
use Illuminate\Database\Seeder;

class TemplateFieldTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //properties
        TemplateField::create(['order' => 1, 'template_id' => 1, 'field_id' => 1]);
        TemplateField::create(['order' => 2, 'template_id' => 1, 'field_id' => 2]);
        TemplateField::create(['order' => 3, 'template_id' => 1, 'field_id' => 3]);
        TemplateField::create(['order' => 4, 'template_id' => 1, 'field_id' => 16]);

        //project
        TemplateField::create(['order' => 1, 'template_id' => 2, 'field_id' => 1]);
        TemplateField::create(['order' => 2, 'template_id' => 2, 'field_id' => 2]);
        TemplateField::create(['order' => 3, 'template_id' => 2, 'field_id' => 42]);
        TemplateField::create(['order' => 4, 'template_id' => 2, 'field_id' => 3]);
        TemplateField::create(['order' => 5, 'template_id' => 2, 'field_id' => 46]);
        TemplateField::create(['order' => 6, 'template_id' => 2, 'field_id' => 16]);
    }
}
