<?php
namespace Database\Seeders;

use App\Models\Template;
use Illuminate\Database\Seeder;

class TemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Template::create([
            'name' => 'Inmuebles',
            'order' => 1,
            'type_node_domain' => Template::PROPERTY
        ]);

        Template::create([
            'name' => 'Proyectos',
            'order' => 2,
            'type_node_domain' => Template::PROJECT
        ]);
    }
}
