<?php
namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //general admin
        User::create([
            'name' => 'Admin',
            'last_name' => 'General',
            'phone' => '923456876',
            'whatsapp' => '923456876',
            'email' => 'general_admin@gmail.com',
            'username' => 'general_admin',
            'password' => bcrypt('123123'),
            'type_domain' => 1,
            'active' => true,
            'date_start' => Carbon::now(),
            'date_end' => Carbon::now(),
            'photo' => 'avatar-1.jpg',
            'role' => User::ROLE_GENERAL_ADMIN
        ]);

        //property admin
        User::create([
            'name' => 'Admin',
            'last_name' => 'Property',
            'phone' => '923456876',
            'whatsapp' => '923456876',
            'email' => 'property_admin@gmail.com',
            'username' => 'property_admin',
            'password' => bcrypt('123123'),
            'type_domain' => 1,
            'active' => true,
            'date_start' => Carbon::now(),
            'date_end' => Carbon::now(),
            'photo' => 'avatar-2.jpg',
            'role' => User::ROLE_PROPERTY_ADMIN
        ]);
    }
}
