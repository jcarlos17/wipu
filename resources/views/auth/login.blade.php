@extends('layouts.auth')

@section('content')
    <div class="m-t-40 card-box">
        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">Iniciar sesión</h4>
        </div>

        <div class="panel-body">
            <form class="form-horizontal" method="POST" action="{{ route('login') }}" autocomplete="off">
                @csrf
                <div class="form-group @error('username') has-error @enderror">
                    <div class="col-xs-12">
                        <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="Usuario" required>
                        @error('username')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>

                <div class="form-group @error('password') has-error @enderror">
                    <div class="col-xs-12">
                        <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña" required autocomplete="off">
                        @error('password')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-custom">
                            <input id="checkbox-signup" type="checkbox">
                            <label for="checkbox-signup">
                                Recordar sesión
                            </label>
                        </div>

                    </div>
                </div>

                <div class="form-group text-center m-t-30">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-bordred btn-block waves-effect waves-light" type="submit">Ingresar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
