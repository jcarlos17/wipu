<div class="col-md-{{ $field->col }}">
    <div class="form-group @error($field->layout_name) has-error @enderror">
        <label for="description">{{ $field->name }}</label>
        <div class="row">
            @foreach($field->options as $option)
                <div class="col-md-{{ $option->col }}">
                    <div class="checkbox checkbox-primary">
                        <input type="checkbox" name="{{ $field->layout_name }}[]"
                               {{ $option->type_domain == \App\Models\Field::CHECKBOX || $option->options->count() > 0 ? 'data-checkbox' : 'data-checked' }}
                               id="{{ $option->id }}" value="{{ $option->name }}"
                            {{ old($field->layout_name, $field->node_field_value($nodeId)) ? (in_array($option->name, old($field->layout_name, $field->node_field_value($nodeId))) ? 'checked' : '') : '' }}>
                        <label for="{{ $option->id }}">
                            @if($option->type_domain == \App\Models\Field::TEXT)
                                <div class="form-inline @error($option->layout_name) has-error @enderror">
                                    <span>{{ $option->name }}</span>
                                    <input type="{{ $option->type }}" class="form-control input-text" id="{{ $option->layout_name }}"
                                           {{ old($field->layout_name, $field->node_field_value($nodeId)) ? (in_array($option->name, old($field->layout_name, $field->node_field_value($nodeId))) ?: 'disabled') : 'disabled' }}
                                           name="{{ $option->layout_name }}" value="{{ old($option->layout_name, $option->node_field_value($nodeId)) }}">
                                    <span>{{ $option->layout_suffix }}</span>
                                    @error($option->layout_name)
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            @else
                                {{ $option->name }}
                            @endif
                        </label>
                        @if($option->type_domain == \App\Models\Field::CHECKBOX || $option->type_domain == \App\Models\Field::TEXT)
                            @foreach($option->options as $subOption)
                                <div class="checkbox checkbox-primary">
                                    <input type="checkbox" name="{{ $field->layout_name }}[]" class="form-check" data-checked
                                           id="{{ $subOption->id }}" value="{{ $subOption->name }}"
                                        {{ old($field->layout_name, $field->node_field_value($nodeId)) ? (in_array($option->name, old($field->layout_name, $field->node_field_value($nodeId))) ?: 'disabled') : 'disabled' }}
                                        @if($subOption->type_domain == \App\Models\Field::TEXT)
                                            {{ old($subOption->layout_name, $subOption->node_field_value($nodeId)) ? 'checked' : '' }}
                                        @else
                                        {{ old($field->layout_name, $field->node_field_value($nodeId)) ? (in_array($subOption->name, old($field->layout_name, $field->node_field_value($nodeId))) ? 'checked' : '') : '' }}
                                        @endif
                                    >
                                    <label for="{{ $subOption->id }}">
                                        @if($subOption->type_domain == \App\Models\Field::TEXT)
                                            <div class="form-inline @error($subOption->layout_name) has-error @enderror">
                                                <span>{{ $subOption->name }}</span>
                                                <input type="{{ $subOption->type }}" class="form-control" id="{{ $subOption->layout_name }}"
                                                       name="{{ $subOption->layout_name }}" value="{{ old($subOption->layout_name, $subOption->node_field_value($nodeId)) }}"
                                                    {{ old($subOption->layout_name, $subOption->node_field_value($nodeId)) ?: 'disabled' }}>
                                                <span>{{ $subOption->layout_suffix }}</span>
                                                @error($subOption->layout_name)
                                                <span class="help-block"><strong>{{ $message }}</strong></span>
                                                @enderror
                                            </div>
                                        @elseif($subOption->type_domain == \App\Models\Field::SELECT)
                                            <div class="form-inline @error($subOption->layout_name) has-error @enderror">
                                                <span>{{ $subOption->name }}</span>
                                                <select name="{{ $subOption->layout_name }}" id="{{ $subOption->layout_name }}" class="form-control"
                                                    {{ old($subOption->layout_name, $subOption->node_field_value($nodeId)) ?: 'disabled' }}>
                                                    <option value="">Seleccione</option>
                                                    @foreach($subOption->options as $label)
                                                        <option value="{{ $label->name }}"
                                                                {{ old($subOption->layout_name, $subOption->node_field_value($nodeId)) == $label->name ? 'selected' : '' }}
                                                        >{{ $label->name }}</option>
                                                    @endforeach
                                                </select>
                                                @error($subOption->layout_name)
                                                <span class="help-block"><strong>{{ $message }}</strong></span>
                                                @enderror
                                            </div>
                                        @else
                                            {{ $subOption->name }}
                                        @endif
                                    </label>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
        @error($field->layout_name)
        <span class="help-block"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>
{!! $field->clearfix !!}
