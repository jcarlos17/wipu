@switch($field->type_domain)
    @case(\App\Models\Field::TEXT)
    @include('includes.fields.text')
    @break

    @case(\App\Models\Field::SELECT)
    @include('includes.fields.select')
    @break

    @case(\App\Models\Field::CHECKBOX)
    @include('includes.fields.checkbox')
    @break

    @case(\App\Models\Field::TEXTAREA)
    @include('includes.fields.textarea')
    @break

    @case(\App\Models\Field::IMAGE)
    @include('includes.fields.image')
    @break

    @case(\App\Models\Field::RADIO)
    @include('includes.fields.radio')
    @break

    @case(\App\Models\Field::MAP)
    @include('includes.fields.map')
    @break
@endswitch
