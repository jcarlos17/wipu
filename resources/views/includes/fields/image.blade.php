<div class="col-md-{{ $field->col }} m-b-10 contentImage">
    <h5 class="font-bold">
        {{ $field->name }}
        <button type="button" data-add class="btn btn-sm btn-success waves-effect waves-light">
            <i class="fa fa-plus"></i>
        </button>
    </h5>
    <div id="contentInputs">
        <div class="form-inline m-b-15 @error($field->layout_name) has-error @enderror">
            <div class="form-group m-r-10">
                <label for="width">Ancho</label>
                <input type="text" class="form-control" id="width"
                       data-max-size="{{ $field->layout_size }}" value="{{ $field->layout_width }}">
            </div>
            <div class="form-group m-r-10">
                <label for="height">Alto</label>
                <input type="text" class="form-control" id="height" value="{{ $field->layout_height }}">
            </div>
            <input type="file" id="images" name="{{ $field->layout_name }}[]" accept="image/*"
                   multiple="multiple" data-image>
            <br>
            <span class="text-danger" id="alert"></span>
            @error($field->layout_name)
            <span class="help-block"><strong>{{ $message }}</strong></span>
            @enderror
        </div>
    </div>
    <template id="template">
        <div class="form-inline m-b-15 @error($field->layout_name) has-error @enderror">
            <div class="form-group m-r-10">
                <label for="width">Ancho</label>
                <input type="text" class="form-control" id="width"
                       data-max-size="{{ $field->layout_size }}" value="{{ $field->layout_width }}">
            </div>
            <div class="form-group m-r-10">
                <label for="height">Alto</label>
                <input type="text" class="form-control" id="height" value="{{ $field->layout_height }}">
            </div>
            <input type="file" id="images" name="{{ $field->layout_name }}[]" accept="image/*"
                   multiple="multiple" {{ $field->layout_name == 'images' ? 'required' : '' }} data-image>
            <br>
            <span class="text-danger" id="alert"></span>
            @error($field->layout_name)
            <span class="help-block"><strong>{{ $message }}</strong></span>
            @enderror
        </div>
    </template>
    <div class="row m-t-20" id="contentImages">
        @if($field->node_field_value($nodeId))
            @foreach($field->node_field_value($nodeId) as $image)
                @if($image)
                    <div class="col-sm-2 position-relative">
                        <input type="hidden" name="value_{{$field->layout_name}}[]" value="{{ $image }}">
                        <img src="{{ asset('images/'.$field->layout_name.'/'.$image) }}" class="img-responsive" alt="">
                        <button data-delete type="button" class="btn btn-sm btn-danger" title="Eliminar" style="position: absolute; top: 0">
                            <i class="fa fa-trash"></i>
                        </button>
                    </div>
                @endif
            @endforeach
        @endif
    </div>
</div>
