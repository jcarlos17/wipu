<div class="col-md-{{ $field->col }}">
    <div id="{{ $field->layout_name }}" style="height: 500px"></div>
    <input type="hidden" name="{{ $field->layout_name }}" value="{{ old($field->layout_name, $field->node_field_value($nodeId)) }}">
    @error($field->layout_name)
    <span class="text-danger"><strong>{{ $message }}</strong></span>
    @enderror
</div>

<script>
    let mymap = L.map({{ $field->layout_name }}).setView([-12.0463731,-77.042754], 6);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1
    }).addTo(mymap);

    const oldValue = {{ old($field->layout_name, $field->node_field_value($nodeId)) ?: '[-12.12, -436.99]' }};

    let marker = L.marker(oldValue).addTo(mymap);

    function onMapClick(e) {
        marker.setLatLng(e.latlng);
        $('input[name="{{ $field->layout_name }}"]').val('['+e.latlng.lat+','+e.latlng.lng+']');
    }
    mymap.on('click', onMapClick);

    function getLatLng(lat, lng) {
        marker.setLatLng([lat,lng]);
        $('input[name="{{ $field->layout_name }}"]').val('['+lat+','+lng+']');
    }
</script>
