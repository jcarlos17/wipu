<ul class="nav nav-tabs nav-justified">
    @foreach($field->options as $option)
    <li role="presentation" class="{{ $option->layout_class }}">
        <a href="#{{ $option->layout_name }}" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">{{ $option->name }}</a>
    </li>
    @endforeach
</ul>
<div class="tab-content">
    @foreach($field->options as $option)
        <div role="tabpanel" class="tab-pane fade {{ $option->layout_class ? 'in active' : '' }}" id="{{ $option->layout_name }}" aria-labelledby="{{ $option->layout_name }}-tab">
            <div class="row">
                @foreach($option->options as $subOption)
                    @include('includes.fields.general', ['field' => $subOption])
                @endforeach
            </div>
        </div>
    @endforeach
</div>
