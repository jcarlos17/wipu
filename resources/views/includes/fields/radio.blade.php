<div class="col-md-{{ $field->col }}">
    <div class="form-group @error($field->layout_name) has-error @enderror">
        <label for="description">{{ $field->name }}</label>
{{--        <input type="hidden" name="field_ids[]" value="{{ $field->id }}">--}}
        <div class="row">
            @foreach($field->options as $option)
                <div class="col-md-{{ $option->col }}">
                    <div class="radio radio-primary">
                        <input type="radio" name="{{ $field->layout_name }}" data-checked id="{{ $option->id }}" value="{{ $option->name }}"
                            {{ old($field->layout_name, $field->node_field_value($nodeId)) == $option->name ? 'checked' : '' }}>
                        <label for="{{ $option->id }}">
                            @if($option->type_domain == \App\Models\Field::TEXT)
                                <div class="form-inline @error($option->layout_name) has-error @enderror">
                                    <span>{{ $option->name }}</span>
                                    <input type="{{ $option->type }}" class="form-control" id="{{ $option->layout_name }}"
                                           name="{{ $option->layout_name }}" value="{{ old($option->layout_name, $option->node_field_value($nodeId)) }}">
                                    <span>{{ $option->layout_suffix }}</span>
                                    @error($option->layout_name)
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            @else
                                {{ $option->name }}
                            @endif
                        </label>
                    </div>
                </div>
            @endforeach
        </div>
        @error($field->layout_name)
        <span class="help-block"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>
{!! $field->clearfix !!}
