<div class="col-md-{{ $field->col }}">
    <div class="form-group @error($field->layout_name) has-error @enderror">
        <label for="{{ $field->layout_name }}">{{ $field->name }}</label>
{{--        <input type="hidden" name="field_ids[]" value="{{ $field->id }}">--}}
        <select name="{{ $field->layout_name }}" id="{{ $field->layout_name }}" onchange="{{$field->layout_name}}Select($(this))" class="form-control">
            <option value="">Seleccione</option>
            @if($provinces && $field->layout_name == 'province')
                @foreach($provinces as $province)
                    <option value="{{ $province->name }}" {{ old($field->layout_name, $field->node_field_value($nodeId)) == $province->name ? 'selected' : '' }}>{{ $province->name }}</option>
                @endforeach
            @elseif($districts && $field->layout_name == 'district')
                @foreach($districts as $district)
                    <option value="{{ $district->name }}" {{ old($field->layout_name, $field->node_field_value($nodeId)) == $district->name ? 'selected' : '' }}>{{ $district->name }}</option>
                @endforeach
            @else
                @foreach($field->data_options as $option)
                    <option value="{{ $option->name }}" {{ old($field->layout_name, $field->node_field_value($nodeId)) == $option->name ? 'selected' : '' }}>{{ $option->name }}</option>
                @endforeach
            @endif
        </select>
        @error($field->layout_name)
        <span class="help-block"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>
{!! $field->clearfix !!}

@if($field->layout_api_url)
    <script>
        const $elements{{$field->layout_name}} = @json($field->layout_disabled_elements);
        const effectedElement{{$field->layout_name}} = @json($field->layout_effected_element);
        const url_prefix{{$field->layout_name}} = @json($field->layout_api_url);

        function {{$field->layout_name}}Select($this) {
            let selectId = $this.val();
            if (! selectId) {
                jQuery.each( $elements{{$field->layout_name}}, function( i, element ) {
                    const $element = $('#'+element);
                    $element.attr('disabled', true);
                    $element.html('<option value="">Seleccione</option>');
                });
                return;
            }
            jQuery.each( $elements{{$field->layout_name}}, function( i, element ) {
                const $element = $('#'+element);
                $element.attr('disabled', true);
                $element.html('<option value="">Seleccione</option>');
            });
            // AJAX
            $.get(url_prefix{{$field->layout_name}}+selectId, function (data_selects) {
                let html_select = '<option value="">Seleccione</option>';

                Array.from(data_selects).forEach(data_select => {
                    html_select += '<option value="'+data_select.name+'">'+data_select.name+'</option>';
                });
                const subSelect = $('#'+effectedElement{{$field->layout_name}});
                subSelect.html(html_select);
                subSelect.attr('disabled', false);
            });

            const $department = $('#department');
            const $province = $('#province');
            const $district = $('#district');

            let address = $district.val()+' '+$province.val()+' '+$department.val();

            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+address+'%20PERU&key=AIzaSyBvuspZieDAMlpAVAe2qwlvkk8oQU34dtg', function(data) {
                const location = data.results[0].geometry.location;

                getLatLng(location.lat, location.lng);
            });
        }
    </script>
@endif
@if($field->layout_name == 'district')
    <script>
        const $elements{{$field->layout_name}} = @json($field->layout_disabled_elements);
        const effectedElement{{$field->layout_name}} = @json($field->layout_effected_element);
        const url_prefix{{$field->layout_name}} = @json($field->layout_api_url);

        function {{$field->layout_name}}Select($this) {
            const $department = $('#department');
            const $province = $('#province');
            const $district = $('#district');

            let address = $district.val()+' '+$province.val()+' '+$department.val();

            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+address+'%20PERU&key=AIzaSyBvuspZieDAMlpAVAe2qwlvkk8oQU34dtg', function(data) {
                const location = data.results[0].geometry.location;

                getLatLng(location.lat, location.lng);
            });
        }
    </script>
@endif
