<div class="col-md-{{ $field->col }}">
    <div class="form-group @error($field->layout_name) has-error @enderror">
        <label for="{{ $field->layout_name }}">{{ $field->name }}</label>
{{--        <input type="hidden" name="field_ids[]" value="{{ $field->id }}">--}}
        <input type="{{ $field->type }}" class="form-control" id="{{ $field->layout_name }}"
               name="{{ $field->layout_name }}" value="{{ old($field->layout_name, $field->node_field_value($nodeId)) }}" placeholder="{{ $field->layout_placeholder }}">
        @error($field->layout_name)
        <span class="help-block"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>
{!! $field->clearfix !!}
