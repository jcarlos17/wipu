<div class="col-md-{{ $field->col }}">
    <div class="form-group @error($field->layout_name) has-error @enderror">
        <label for="{{ $field->layout_name }}">{{ $field->name }}</label>
{{--        <input type="hidden" name="field_ids[]" value="{{ $field->id }}">--}}
        <textarea id="{{ $field->layout_name }}" name="{{ $field->layout_name }}">{{ old($field->layout_name, $field->node_field_value($nodeId)) }}</textarea>
        @error($field->layout_name)
        <span class="help-block"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>
{!! $field->clearfix !!}
