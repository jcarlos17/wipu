<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{ config('app.name') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <title>{{ config('app.name') }}</title>

    @yield('styles')

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/core.css') }}" rel="stylesheet">
    <link href="{{ asset('css/components.css') }}" rel="stylesheet">
    <link href="{{ asset('css/icons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/pages.css') }}" rel="stylesheet">
    <link href="{{ asset('css/menu.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">


    <script src="{{ asset('js/modernizr.min.js') }}"></script>
</head>


<body class="fixed-left">

<div id="wrapper">

    <div class="topbar">
        <div class="topbar-left">
            <a href="#" class="logo"><span>Wipu</span><i class="zmdi zmdi-layers"></i></a>
        </div>
        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <!-- Page title -->
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <button class="button-menu-mobile open-left">
                            <i class="zmdi zmdi-menu"></i>
                        </button>
                    </li>
                    <li class="hidden-xs">
                        <h4 class="page-title">@yield('section_title')</h4>
                    </li>
                </ul>
                {{--<ul class="nav navbar-nav navbar-right">--}}
                {{--<li class="hidden-xs">--}}
                {{--<form role="search" class="app-search">--}}
                {{--<input type="text" placeholder="Search..."--}}
                {{--class="form-control">--}}
                {{--<a href=""><i class="fa fa-search"></i></a>--}}
                {{--</form>--}}
                {{--</li>--}}
                {{--</ul>--}}
            </div><!-- end container -->
        </div><!-- end navbar -->
    </div>

@include('includes.menu')

<!-- Start right Content here -->
    <div class="content-page">

        <div class="content">
            <div class="container">

                @yield('content')
            </div> <!-- container -->

        </div>

        <footer class="footer">
            <div class="row">
                <div class="col-sm-6">
                    <script>document.write(new Date().getFullYear())</script> &copy; Todos los derechos reservados. {{ config('app.name') }}

                </div>
            </div>
        </footer>

    </div>
    <!-- End Right content here -->

</div>
<!-- END wrapper -->

<script>
    var resizefunc = [];
</script>
{{--<script src="{{ mix('js/app.js') }}"></script>--}}

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/detect.js') }}"></script>
<script src="{{ asset('js/fastclick.js') }}"></script>
<script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('js/waves.js') }}"></script>
<script src="{{ asset('js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('js/jquery.scrollTo.min.js') }}"></script>

@yield('scripts')

<script src="{{ asset('js/jquery.core.js') }}"></script>
<script src="{{ asset('js/jquery.app.js') }}"></script>
</body>
</html>
