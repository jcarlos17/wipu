@extends('layouts.app')

@section('section_title')
    {{ $node->name }}
@endsection

@section('styles')
    <!-- X-editable css -->
    <link type="text/css" href="{{ asset('plugins/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="">
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
@endsection
@section('content')
    @include('includes.alert')
    <form role="form" action="" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-box">
            <div class="row">
                @foreach($template->fields as $field)
                    <ul class="nav nav-tabs nav-justified">
                        @foreach($field->options as $option)
                            <li role="presentation" class="{{ $option->layout_class }}">
                                <a href="#{{ $option->layout_name }}" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">{{ $option->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($field->options as $option)
                            <div role="tabpanel" class="tab-pane fade {{ $option->layout_class ? 'in active' : '' }}" id="{{ $option->layout_name }}" aria-labelledby="{{ $option->layout_name }}-tab">
                                <div class="row">
                                    @foreach($option->options as $subOption)
                                        @include('includes.fields.general', ['field' => $subOption, 'nodeId' => $node->id])
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-rounded btn-bordred waves-effect waves-light w-md">
                Guardar
            </button>
            <a href="#" class="btn btn-danger btn-rounded btn-bordred waves-effect waves-light w-md">Cancelar</a>
        </div>
    </form>
@endsection

@section('scripts')
    <!-- Form wizard -->
    <script src="{{ asset('plugins/bootstrap-wizard/jquery.bootstrap.wizard.js') }}"></script>
    <script src="{{ asset('plugins/jquery-validation/dist/jquery.validate.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#basicwizard').bootstrapWizard({'tabClass': 'nav nav-tabs navtab-wizard nav-justified bg-muted'});
        });
    </script>

    <!--form wysiwig js-->
    <script src="{{ asset('plugins/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if($("#description").length > 0){
                tinymce.init({
                    selector: "textarea#description",
                    theme: "modern",
                    height:300,
                    plugins: [
                        "advlist autolink link lists charmap preview hr anchor pagebreak",
                        "searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview | forecolor backcolor",
                    style_formats: [
                        {title: 'Bold text', inline: 'b'},
                        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                        {title: 'Example 1', inline: 'span', classes: 'example1'},
                        {title: 'Example 2', inline: 'span', classes: 'example2'},
                        {title: 'Table styles'},
                        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                    ]
                });
            }
        });
    </script>

    <!-- google maps api -->
    <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <!-- main file -->
    <script src="{{ asset('plugins/gmaps/gmaps.min.js') }}"></script>
    <!-- demo codes -->
    <script src="{{ asset('pages/jquery.gmaps.js') }}"></script>
    <script>
        $(document).on('change', '[data-checked]', function () {
            const checked = $(this).is(':checked');

            let $div = $(this).closest('.checkbox-primary');

            let $input = $div.find('.form-control');

            if(checked) {
                $input.attr('disabled', false)
                $input.attr('required', true)
            } else {
                $input.attr('disabled', true)
                $input.val('');
                $input.attr('required', false)
            }
        });

        $(document).on('change', '[data-checkbox]', function () {
            const checked = $(this).is(':checked');

            let $div = $(this).closest('.checkbox-primary');

            let $textInput = $div.find('.input-text');
            let $input = $div.find('.form-control')
            let $checkboxes = $div.find('.form-check');

            if(checked) {
                $checkboxes.attr('disabled', false)
                $textInput.attr('disabled', false)
                $input.attr('required', true)
            } else {
                $input.attr('disabled', true)
                $input.val('');
                $input.attr('required', false)
                $checkboxes.attr('disabled', true)
                $checkboxes.removeAttr('checked')
            }
        });
    </script>
    <script>
        $(document).on('click', '[data-add]', function () {
            let $div = $(this).closest('.contentImage');
            const contentInputs = $div.find('#contentInputs');
            const $template = $div.find('#template');

            contentInputs.append($template.html());
        });

        $(document).on('change', '[data-image]', function () {
            let $div = $(this).closest('.form-inline');

            let $width = $div.find('#width');
            let $height = $div.find('#height');
            let $alert = $div.find('#alert');
            let maxSize = $width.data('max-size');
            let content = $div.find('#contentImages');

            let fileData = $(this).prop("files");

            Array.from(fileData).forEach(fileData => {
                let size = fileData.size/1024;

                if (maxSize < size) {
                    $alert.text('Algunas imágenes exceden el tamaño máximo');
                    $(this).val('');
                }

                const img = new Image();

                img.src = window.URL.createObjectURL(fileData);
                img.onload = () => {
                    const width = img.naturalWidth;
                    const height = img.naturalHeight;
                    if (width != $width.val() || height != $height.val()) {
                        $alert.text('Algunas imágenes no tienen las medidas indicadas');
                        $(this).val('');
                    }
                };
            });

            $alert.text('');
        });

        $(document).on('click', '[data-delete]', function () {
            let $div = $(this).closest('.col-sm-2');
            $div.remove();
        });
    </script>
@endsection
