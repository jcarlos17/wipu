@extends('layouts.app')

@section('section_title')
    {{ $template->name }}
@endsection

@section('styles')

@endsection

@section('content')
    @include('includes.alert')
    <div class="row">
        <div class="col-sm-8">
{{--            <a href="{{ url('admin/trainings/'.$training->id.'/earning/create') }}"--}}
{{--               class="btn btn-success btn-rounded btn-bordred waves-effect waves-light w-md m-b-15 m-r-10">--}}
{{--                Nuevo ingreso--}}
{{--            </a>--}}
        </div><!-- end col -->
    </div>
    <div class="row">
        <div class="col-sm-3">
           @include('nodes.search')
        </div>
        <div class="col-sm-9">
            <div class="card-box table-responsive m-t-20">
                <h4 class="header-title m-t-0 m-b-30">{{ $template->name }}</h4>
                <table class="table table-hover">
                    <tbody>
                    @foreach($nodes as $node)
                        <tr class="row">
                            <td class="col-sm-2"></td>
                            <td class="col-sm-10">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <h4 class="font-bold"><a href="#">{{ $node->node_field('Título', \App\Models\Field::TEXT)->value }}</a></h4>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="text-right">
                                            <button type="button" class="btn btn-sm btn-primary dropdown-toggle waves-effect waves-light"
                                                    data-toggle="dropdown" aria-expanded="false">Más acciones <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                                <li><a href="{{ url('nodes/'.$node->id) }}">Editar</a></li>
                                                <li><a href="#">URL corta</a></li>
                                                <li><a href="#">URL Alternas</a></li>
                                                <li><a href="#">Ver inmueble</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Eliminar</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <p class="font-bold">{{ $node->node_field('Dirección', \App\Models\Field::TEXT)->value }}</p>
                                        <p>{{ $node->node_field('Departamento', \App\Models\Field::SELECT)->value }},
                                            {{ $node->node_field('Provincia', \App\Models\Field::SELECT)->value }} ,
                                            {{ $node->node_field('Distrito', \App\Models\Field::SELECT)->value }}</p>
                                        <p>Alquiler / S/. 2,200 ó US$ 0</p>
                                    </div>
                                    <div class="col-sm-2 text-center">
                                        <i class="fa fa-2x fa-meh-o"></i><br>
                                        <small>Completo al 80%</small>
                                        <div class="progress progress-bar-primary-alt">
                                            <div class="progress-bar progress-bar-primary" role="progressbar"
                                                 aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 text-center">
                                        <i class="fa fa-2x fa-users"></i><br>
                                        <small>1 Visita al aviso</small>
                                    </div>
                                    <div class="col-sm-2 text-center">
                                        <i class="fa fa-2x fa-mobile"></i><br>
                                        <small>0 Interesados en contactarte</small>
                                    </div>
                                    <div class="col-sm-2 text-center">
                                        <i class="fa fa-2x fa-envelope"></i><br>
                                        <small>0 consultas</small><br>
                                        <small><a href="#">0 sin responder</a></small>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr class="text-white" style="background: #6c757d">
                                            <td>
                                                ID: {{ $node->id }} | Creación: {{ $node->created_at }}
                                            </td>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $nodes->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        const $department = $('#department');
        const $province = $('#province');
        const $district = $('#district');

        $(function() {
            $department.on('change', onSelectDepartmentChange);
            $province.on('change', onSelectProvinceChange);
        });

        function onSelectDepartmentChange() {
            let departmentId = $(this).val();
            if (! departmentId) {
                $province.html('<option value="">Seleccione</option>');
                $district.html('<option value="">Seleccione</option>');
                $province.attr('disabled', true);
                $district.attr('disabled', true);
                return;
            }

            $province.html('<option value="">Seleccione</option>');
            $district.html('<option value="">Seleccione</option>');
            $province.attr('disabled', true);
            $district.attr('disabled', true);

            // AJAX
            $.get('/api/department/provinces/'+departmentId, function (provinces) {
                let html_select = '<option value="">Seleccione</option>';

                Array.from(provinces).forEach(province => {
                    html_select += '<option value="'+province.name+'">'+province.name+'</option>';
                });
                $province.html(html_select);
                $province.attr('disabled', false);
            });
        }

        function onSelectProvinceChange() {
            let provinceId = $(this).val();

            if (! provinceId) {
                $district.html('<option value="">Seleccione</option>');
                $district.attr('disabled', true);
                return;
            }

            $.get('/api/province/districts/'+provinceId, function (districts) {
                let html_select = '<option value="">Seleccione</option>';

                Array.from(districts).forEach(district => {
                    html_select += '<option value="'+district.name+'">'+district.name+'</option>';
                });
                $district.html(html_select);
                $district.attr('disabled', false)
            });
        }
    </script>
@endsection
