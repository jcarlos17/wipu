<div class="card-box m-t-20">
    <form method="GET">
        <button type="submit" class="btn btn-block btn-success m-b-20"><i class="fa fa-search"></i> Aplicar</button>
        <div class="form-group">
            <input type="text" class="form-control" name="title" placeholder="Ingrese Título o ID">
        </div>

        <h3 class="card-title">Fecha de Creación</h3>
        <hr class="m-t-10">
        <div class="radio radio-primary">
            <input type="radio" name="created_at" id="other_date" value="other_date">
            <label for="other_date">Otra fecha</label>
        </div>
        <div class="radio radio-primary">
            <input type="radio" name="created_at" id="month_3" value="month_3">
            <label for="month_3">Últimos 3 meses</label>
        </div>
        <div class="radio radio-primary">
            <input type="radio" name="created_at" id="month_2" value="month_2">
            <label for="month_2">Últimos 2 meses</label>
        </div>
        <div class="radio radio-primary">
            <input type="radio" name="created_at" id="month_1" value="month_1">
            <label for="month_1">Último mes</label>
        </div>
        <div class="radio radio-primary">
            <input type="radio" name="created_at" id="week_2" value="week_2">
            <label for="week_2">Últimas 2 semanas</label>
        </div>
        <div class="radio radio-primary">
            <input type="radio" name="created_at" id="week_1" value="week_1">
            <label for="week_1">Última semana</label>
        </div>

        <h3 class="card-title">Estado</h3>
        <hr class="m-t-10">
        <div class="checkbox checkbox-primary">
            <input type="checkbox" name="state" id="active" value="1">
            <label for="active">Activo</label>
        </div>
        <div class="checkbox checkbox-primary">
            <input type="checkbox" name="state" id="inactive" value="0">
            <label for="inactive">Inactivo</label>
        </div>

        <h3 class="card-title">Transacción</h3>
        <hr class="m-t-10">
        <div class="checkbox checkbox-primary">
            <input type="checkbox" name="state" id="sale" value="sale">
            <label for="sale">Venta</label>
        </div>
        <div class="checkbox checkbox-primary">
            <input type="checkbox" name="state" id="rental" value="rental">
            <label for="rental">Alquiler</label>
        </div>
        <div class="checkbox checkbox-primary">
            <input type="checkbox" name="state" id="season" value="season">
            <label for="season">Temporada</label>
        </div>
        <div class="checkbox checkbox-primary">
            <input type="checkbox" name="state" id="transfer" value="transfer">
            <label for="transfer">Traspaso</label>
        </div>

        <h3 class="card-title">Localización</h3>
        <hr class="m-t-10">
        <div class="form-group">
            <label for="department">Departamento</label>
            <select class="form-control" name="department" id="department">
                <option value="">Seleccione</option>
                @foreach($departments as $department)
                    <option value="{{ $department->name }}">{{ $department->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="province">Provincia</label>
            <select class="form-control" name="province" id="province" disabled>
                <option value="">Seleccione</option>
            </select>
        </div>
        <div class="form-group">
            <label for="district">Distrito</label>
            <select class="form-control" name="district" id="district" disabled>
                <option value="">Seleccione</option>
            </select>
        </div>

        <h3 class="card-title">Inmueble</h3>
        <hr class="m-t-10">
        <div class="form-group">
            <label for="type">Tipo</label>
            <select class="form-control" name="type" id="type">
                <option value="">Seleccione</option>
            </select>
        </div>

        <h3 class="card-title">Otros</h3>
        <hr class="m-t-10">
        <div class="form-group">
            <label for="bedroom">Dormitorios</label>
            <select class="form-control" name="bedroom" id="bedroom">
                <option value="">Seleccione</option>
            </select>
        </div>
        <div class="form-group">
            <label for="toilet">Baños</label>
            <select class="form-control" name="toilet" id="toilet">
                <option value="">Seleccione</option>
            </select>
        </div>
        <div class="form-group">
            <label for="parking">Estacionamientos</label>
            <select class="form-control" name="parking" id="parking">
                <option value="">Seleccione</option>
            </select>
        </div>
    </form>
</div>
