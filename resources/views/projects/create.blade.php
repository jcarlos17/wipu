@extends('layouts.app')

@section('section_title', 'Agregar proyecto')

@section('styles')
    <!-- X-editable css -->
    <link type="text/css" href="{{ asset('plugins/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css') }}" rel="stylesheet">
@endsection
@section('content')
    @include('includes.alert')
    <form role="form" action="" method="POST">
        @csrf
        <div class="card-box">
            <div id="basicwizard" class=" pull-in">
                <ul>
                    <li><a href="#tab1" data-toggle="tab">Datos principales</a></li>
                    <li><a href="#tab2" data-toggle="tab">Características</a></li>
                    <li><a href="#tab3" data-toggle="tab">Multimedia</a></li>
                    <li><a href="#tab4" data-toggle="tab">Asesor</a></li>
                    <li><a href="#tab5" data-toggle="tab">Contacto</a></li>
                </ul>
                <div class="tab-content b-0 m-b-0">
                    <div class="tab-pane m-t-10 fade" id="tab1">
                        <div class="form-group @error('title') has-error @enderror">
                            <label for="title">Título</label>
                            <input type="text" name="title" class="form-control" id="title" value="{{ old('title') }}"
                                   placeholder="Incluya el tipo de propiedad y su característica principal">
                            @error('title')
                            <span class="help-block"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                        <div class="form-group @error('description') has-error @enderror">
                            <label for="description">Descripción</label>
                            <textarea id="description" name="description">{{ old('description') }}</textarea>
                            @error('description')
                            <span class="help-block"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group @error('stage') has-error @enderror">
                                    <label for="stage">Etapa del proyecto</label>
                                    <select class="form-control" name="stage" id="stage">
                                        <option value="">Seleccione</option>
                                        <option value="1">Pre-ventas en planos</option>
                                        <option value="2">Pre-ventas en construcción</option>
                                        <option value="3">Venta en estreno</option>
                                    </select>
                                    @error('stage')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group @error('delivery') has-error @enderror">
                                    <label for="delivery">Entrega</label>
                                    <select class="form-control" name="delivery" id="delivery">
                                        <option value="">Seleccione</option>
                                        <option value="1">1° Trimestre</option>
                                        <option value="2">2° Trimestre</option>
                                        <option value="3">3° Trimestre</option>
                                        <option value="4">4° Trimestre</option>
                                        <option value="5">Enero</option>
                                        <option value="6">Febrero</option>
                                        <option value="7">Marzo</option>
                                        <option value="8">Abril</option>
                                        <option value="9">Mayo</option>
                                        <option value="10">Junio</option>
                                        <option value="11">Julio</option>
                                        <option value="12">Agosto</option>
                                        <option value="13">Setiembre</option>
                                        <option value="14">Octubre</option>
                                        <option value="15">Noviembre</option>
                                        <option value="16">Diciembre</option>
                                    </select>
                                    @error('delivery')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group @error('year') has-error @enderror">
                                    <label for="year">Año de entrega (YYYY)</label>
                                    <input type="number" class="form-control" name="year" id="year">
                                    @error('year')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label for="description">Tipo de inmueble</label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="radio radio-primary">
                                        <input type="radio" name="radio" id="radio1" value="option1">
                                        <label for="radio1">
                                            Casa
                                        </label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input type="radio" name="radio" id="radio03" value="option3">
                                        <label for="radio03">
                                            Habitación
                                        </label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input type="radio" name="radio" id="radio3" value="option3">
                                        <label for="radio3">
                                            Local Industrial
                                        </label>
                                    </div>
                                </div><!-- end col -->
                                <div class="col-sm-3">
                                    <div class="radio radio-primary">
                                        <input type="radio" name="radio" id="radio4" value="option4">
                                        <label for="radio4">
                                            Casa de campo
                                        </label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input type="radio" name="radio" id="radio5" value="option5">
                                        <label for="radio5">
                                            Oficina
                                        </label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input type="radio" name="radio" id="radio6" value="option6">
                                        <label for="radio6">
                                            Terreno agrícola
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="radio radio-primary">
                                        <input type="radio" name="property_type" id="radio7" value="option7">
                                        <label for="radio7">
                                            Casa de playa
                                        </label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input type="radio" name="property_type" id="radio8" value="option8">
                                        <label for="radio8">
                                            Terreno / Lote
                                        </label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input type="radio" name="property_type" id="radio9" value="option9">
                                        <label for="radio9">
                                            Negocio en marcha
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="radio radio-primary">
                                        <input type="radio" name="property_type" id="radio10" value="option10">
                                        <label for="radio10">
                                            Departamento
                                        </label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input type="radio" name="property_type" id="radio11" value="option11">
                                        <label for="radio11">
                                            Local Comercial
                                        </label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input type="radio" name="property_type" id="radio12" value="option12">
                                        <label for="radio12">
                                            Otros
                                        </label>
                                    </div>
                                </div><!-- end col -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group @error('bedrooms') has-error @enderror">
                                    <label for="bedrooms">Dormitorios</label>
                                    <input type="number" class="form-control" name="bedrooms" id="bedrooms">
                                    @error('bedrooms')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group @error('parking_lots') has-error @enderror">
                                    <label for="parking_lots">Estacionamientos</label>
                                    <input type="number" class="form-control" name="parking_lots" id="parking_lots">
                                    @error('parking_lots')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group @error('bathrooms') has-error @enderror">
                                    <label for="bathrooms">Baños</label>
                                    <input type="number" class="form-control" name="bathrooms" id="bathrooms">
                                    @error('bathrooms')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group @error('half_bath') has-error @enderror">
                                    <label for="half_bath">Medio baño</label>
                                    <input type="number" class="form-control" name="half_bath" id="half_bath">
                                    @error('half_bath')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-2">
                                <div class="form-group @error('number_beds') has-error @enderror">
                                    <label for="number_beds">N° Camas</label>
                                    <input type="number" class="form-control" name="number_beds" id="number_beds">
                                    @error('number_beds')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group @error('number_floors') has-error @enderror">
                                    <label for="number_floors">N° Pisos</label>
                                    <input type="number" class="form-control" name="number_floors" id="number_floors">
                                    @error('number_floors')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group @error('number_departments') has-error @enderror">
                                    <label for="number_departments">N° Departamentos</label>
                                    <input type="number" class="form-control" name="number_departments" id="number_departments">
                                    @error('number_departments')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group @error('pen_price_from') has-error @enderror">
                                    <label for="pen_price_from">Precios desde S/.</label>
                                    <input type="number" class="form-control" name="pen_price_from" id="pen_price_from">
                                    @error('pen_price_from')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group @error('usd_price_from') has-error @enderror">
                                    <label for="usd_price_from">Precio desde US$</label>
                                    <input type="number" class="form-control" name="usd_price_from" id="usd_price_from">
                                    @error('usd_price_from')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-2">
                                <div class="form-group @error('min_total_area') has-error @enderror">
                                    <label for="min_total_area">Área total mínima</label>
                                    <input type="number" class="form-control" name="min_total_area" id="min_total_area">
                                    @error('min_total_area')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group @error('max_total_area') has-error @enderror">
                                    <label for="max_total_area">Área total máxima</label>
                                    <input type="number" class="form-control" name="max_total_area" id="max_total_area">
                                    @error('max_total_area')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-2">
                                <div class="form-group @error('min_covered_area') has-error @enderror">
                                    <label for="min_covered_area">Área techada mínima</label>
                                    <input type="number" class="form-control" name="min_covered_area" id="min_covered_area">
                                    @error('min_covered_area')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group @error('max_covered_area') has-error @enderror">
                                    <label for="max_covered_area">Área techada máxima</label>
                                    <input type="number" class="form-control" name="max_covered_area" id="max_covered_area">
                                    @error('max_covered_area')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group @error('department') has-error @enderror">
                                    <label for="department">Departamento</label>
                                    <select class="form-control" name="department" id="department">
                                        <option value="">Seleccione</option>
                                    </select>
                                    @error('department')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group @error('province') has-error @enderror">
                                    <label for="province">Provincia</label>
                                    <select class="form-control" name="province" id="province">
                                        <option value="">Seleccione</option>
                                    </select>
                                    @error('province')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group @error('district') has-error @enderror">
                                    <label for="district">Distrito</label>
                                    <select class="form-control" name="district" id="district">
                                        <option value="">Seleccione</option>
                                    </select>
                                    @error('district')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-6">
                                <div class="form-group @error('address') has-error @enderror">
                                    <label for="address">Dirección</label>
                                    <input type="text" class="form-control" name="address" id="address">
                                    @error('address')
                                    <span class="help-block"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div id="gmaps-markers" class="gmaps"></div>
                        </div>

                    </div>
                    <div class="tab-pane m-t-10 fade" id="tab2">

                    </div>
                    <div class="tab-pane m-t-10 fade" id="tab3">

                    </div>
                    <div class="tab-pane m-t-10 fade" id="tab4">

                    </div>
                    <div class="tab-pane m-t-10 fade" id="tab5">

                    </div>
                    <ul class="pager wizard m-b-0">
                        <li class="previous"><a href="#" class="btn btn-primary waves-effect waves-light">Anterior</a>
                        </li>
                        <li class="next"><a href="#" class="btn btn-primary waves-effect waves-light">Siguiente</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="button" class="btn btn-primary btn-rounded btn-bordred waves-effect waves-light w-md">
                Guardar
            </button>
            <a href="#" class="btn btn-danger btn-rounded btn-bordred waves-effect waves-light w-md">Cancelar</a>
        </div>
    </form>
@endsection

@section('scripts')
    <!-- Form wizard -->
    <script src="{{ asset('plugins/bootstrap-wizard/jquery.bootstrap.wizard.js') }}"></script>
    <script src="{{ asset('plugins/jquery-validation/dist/jquery.validate.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#basicwizard').bootstrapWizard({'tabClass': 'nav nav-tabs navtab-wizard nav-justified bg-muted'});
        });
    </script>

    <!--form wysiwig js-->
    <script src="{{ asset('plugins/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if($("#description").length > 0){
                tinymce.init({
                    selector: "textarea#description",
                    theme: "modern",
                    height:300,
                    plugins: [
                        "advlist autolink link lists charmap preview hr anchor pagebreak",
                        "searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview | forecolor backcolor",
                    style_formats: [
                        {title: 'Bold text', inline: 'b'},
                        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                        {title: 'Example 1', inline: 'span', classes: 'example1'},
                        {title: 'Example 2', inline: 'span', classes: 'example2'},
                        {title: 'Table styles'},
                        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                    ]
                });
            }
        });
    </script>

    <!-- google maps api -->
    <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <!-- main file -->
    <script src="{{ asset('plugins/gmaps/gmaps.min.js') }}"></script>
    <!-- demo codes -->
    <script src="{{ asset('pages/jquery.gmaps.js') }}"></script>
@endsection
