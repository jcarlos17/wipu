@extends('layouts.app')

@section('section_title', 'Mi portal')

@section('styles')
    <!-- form Uploads -->
    <link href="{{ asset('plugins/fileuploads/css/dropify.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    @include('includes.alert')
    <form role="form" action="" method="POST">
        @csrf
        <div class="row">
            <div class="col-sm-6">
                <div class="card-box">
                    <h4 class="m-t-0 m-b-15 header-title"><b>Mis datos</b></h4>
                    <div class="form-group">
                        <label for="username">Usuario</label>
                        <input type="text" name="username" class="form-control" id="username" value="{{ $user->username }}" disabled>
                    </div>
                    <div class="form-group @error('name') has-error @enderror">
                        <label for="name">Nombre</label>
                        <input type="text" name="name" class="form-control" id="name" value="{{ old('name', $user->name) }}">
                        @error('name')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group @error('last_name') has-error @enderror">
                        <label for="last_name">Apellidos</label>
                        <input type="text" name="last_name" class="form-control" id="last_name" value="{{ old('last_name', $user->last_name) }}">
                        @error('last_name')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group @error('email') has-error @enderror">
                        <label for="email">E-mail</label>
                        <input type="email" name="email" class="form-control" id="email" value="{{ old('email', $user->email) }}">
                        @error('email')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group @error('phone') has-error @enderror">
                        <label for="phone">Teléfono/Móvil</label>
                        <input type="text" name="phone" class="form-control" id="phone" value="{{ old('phone', $user->phone) }}">
                        @error('phone')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group @error('address') has-error @enderror">
                        <label for="address">Dirección</label>
                        <input type="text" name="address" class="form-control" id="address" value="{{ old('address', $user->address) }}">
                        @error('address')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group @error('facebook') has-error @enderror">
                        <label for="facebook">Facebook</label>
                        <input type="text" name="facebook" class="form-control" id="facebook" value="{{ old('facebook', $user->facebook) }}">
                        @error('facebook')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group @error('whatsapp') has-error @enderror">
                        <label for="whatsapp">Whatsapp</label>
                        <input type="text" name="whatsapp" class="form-control" id="whatsapp" value="{{ old('whatsapp', $user->whatsapp) }}">
                        @error('whatsapp')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group @error('ruc') has-error @enderror">
                        <label for="ruc">RUC</label>
                        <input type="text" name="ruc" class="form-control" id="ruc" value="{{ old('ruc', $user->ruc) }}">
                        @error('ruc')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group @error('dni') has-error @enderror">
                        <label for="dni">DNI</label>
                        <input type="text" name="dni" class="form-control" id="dni" value="{{ old('dni', $user->dni) }}">
                        @error('dni')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group @error('business_name') has-error @enderror">
                        <label for="business_name">Razón social</label>
                        <input type="text" name="business_name" class="form-control" id="business_name" value="{{ old('business_name', $user->business_name) }}">
                        @error('business_name')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group @error('description') has-error @enderror">
                        <label for="description">Descripción</label>
                        <textarea class="form-control" name="description" id="description" cols="30" rows="5">{{ old('description', $user->description) }}</textarea>
                        @error('description')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card-box">
                    <h4 class="m-t-0 m-b-15 header-title"><b>Portal</b></h4>
                    <div class="form-group @error('title') has-error @enderror">
                        <label for="title">Título</label>
                        <input type="text" name="title" class="form-control" id="title" value="{{ old('title') }}">
                        @error('title')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group @error('description') has-error @enderror">
                        <label for="description">Descripción</label>
                        <textarea class="form-control" name="description" id="description" cols="30" rows="5">{{ old('description', $user->description) }}</textarea>
                        @error('description')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="card-box">
                    <h4 class="m-t-0 m-b-15 header-title"><b>SEO</b></h4>
                    <div class="form-group @error('seo_title') has-error @enderror">
                        <label for="seo_title">Meta Title</label>
                        <input type="text" name="seo_title" class="form-control" id="seo_title" value="{{ old('seo_title') }}">
                        @error('seo_title')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group @error('seo_description') has-error @enderror">
                        <label for="seo_description">Meta Description</label>
                        <textarea class="form-control" name="seo_description" id="seo_description" cols="30" rows="5">{{ old('seo_description') }}</textarea>
                        @error('seo_description')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group @error('seo_tags') has-error @enderror">
                        <label for="seo_tags">Tags</label>
                        <input type="text" name="seo_tags" class="form-control" id="seo_tags" value="{{ old('seo_tags') }}">
                        @error('seo_tags')
                        <span class="help-block"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>

                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">Ubicación de contacto</h4>

                    <div id="gmaps-markers" class="gmaps"></div>
                </div>

                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">Mi galería</h4>

                    <input type="file" class="dropify" data-height="100" />
                    <div class="row m-t-15">
                        <div class="col-sm-3">
                            <img src="{{ asset('images/gallery/1.jpg') }}" class="thumb-img" alt="work-thumbnail">
                        </div>
                        <div class="col-sm-3">
                            <img src="{{ asset('images/gallery/2.jpg') }}" class="thumb-img" alt="work-thumbnail">
                        </div>
                        <div class="col-sm-3">
                            <img src="{{ asset('images/gallery/3.jpg') }}" class="thumb-img" alt="work-thumbnail">
                        </div>
                        <div class="col-sm-3">
                            <img src="{{ asset('images/gallery/5.jpg') }}" class="thumb-img" alt="work-thumbnail">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="button" class="btn btn-primary btn-rounded btn-bordred waves-effect waves-light w-md">
                Guardar
            </button>
            <a href="#" class="btn btn-danger btn-rounded btn-bordred waves-effect waves-light w-md">Cancelar</a>
        </div>
    </form>
@endsection

@section('scripts')
    <!-- google maps api -->
    <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <!-- main file -->
    <script src="{{ asset('plugins/gmaps/gmaps.min.js') }}"></script>
    <!-- demo codes -->
    <script src="{{ asset('pages/jquery.gmaps.js') }}"></script>

    <!-- file uploads js -->
    <script src="{{ asset('plugins/fileuploads/js/dropify.min.js') }}"></script>

    <script type="text/javascript">
        $('.dropify').dropify({
            messages: {
                'default': 'Drag and drop a file here or click',
                'replace': 'Drag and drop or click to replace',
                'remove': 'Remove',
                'error': 'Ooops, something wrong appended.'
            },
            error: {
                'fileSize': 'The file size is too big (1M max).'
            }
        });
    </script>
@endsection
