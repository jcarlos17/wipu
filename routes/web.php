<?php

use App\Http\Controllers\Api\LocationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NodeController;
use App\Http\Controllers\System\MyPortalController;
use App\Http\Controllers\System\ProjectController;
use App\Http\Controllers\TemplateController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth'], 'namespace' => 'System'], function (){
    //My Portal
    Route::get('my-portal', [MyPortalController::class, 'index']);
    //projects
    Route::group(['prefix' => 'projects'], function (){
        Route::get('create', [Projectcontroller::class, 'create']);
    });
});

Route::group(['middleware' => ['auth']], function (){
    //Template
    Route::get('templates/{template}', [TemplateController::class, 'index']);
    Route::get('templates/{template}/create', [TemplateController::class, 'create']);
    Route::post('templates/{template}/create', [TemplateController::class, 'store']);

    Route::get('nodes/{node}', [NodeController::class, 'edit']);
    Route::post('nodes/{node}', [NodeController::class, 'update']);
});

//api
Route::group(['prefix' => 'api'], function (){
    //provinces
    Route::get('department/provinces/{department:name}', [LocationController::class, 'provinces']);
    Route::get('province/districts/{province:name}', [LocationController::class, 'districts']);
});
